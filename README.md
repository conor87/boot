# README #

Note: This README is used for both boot and boot2.

Both can be built using: mvn clean install

Before running rabbitmq should be setup on default ports. I used this compose file:
rabbitmq:
  image: rabbitmq:management
  ports:
    - "5672:5672"
    - "15672:15672"

### Boot:
 - run: sudo java -jar target/boot-1.0-SNAPSHOT.jar --server.port=80
 
### Boot2:
 - run: java -jar target/boot2-1.0-SNAPSHOT.jar
 
### Testing:
 - curl http://localhost:80/greeting
 - curl -u admin:admin http://localhost:8080/internal-greetings
 - curl http://localhost:8082/report   <-- sends GET rest request to boot /greeting
 - curl -H "Content-Type: application/json" -X POST -d '{"message":"sending message"}' http://localhost:8082/report <-- sends message to rabbitmq and consumed by boot



