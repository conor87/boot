package com.test.user;

import com.test.model.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

  public static final String HELLO_WORLD = "Hello World";

  @RequestMapping(value = "/greeting", method = RequestMethod.GET)
  public Greeting getGreeting(@RequestParam(value="name", defaultValue= HELLO_WORLD) String message){
    return new Greeting(message);
  }

}
