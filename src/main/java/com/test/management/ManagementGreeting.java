package com.test.management;

import com.test.model.Greeting;
import org.springframework.boot.actuate.endpoint.Endpoint;
import org.springframework.stereotype.Component;

@Component
public class ManagementGreeting implements Endpoint<Greeting> {

  public static final String HELLO_MANAGEMENT = "Hello Management";

  public String getId() {
    return "internal-greetings";
  }

  public boolean isEnabled() {
    return true;
  }

  public boolean isSensitive() {
    return true;
  }

  public Greeting invoke() {
    return new Greeting(HELLO_MANAGEMENT);
  }
}