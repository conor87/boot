package com.test.rabbit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class GreetingReceiver {

  private static final Logger LOGGER = LoggerFactory.getLogger(GreetingReceiver.class);

  public void receiveGreeting(String greeting) {

    LOGGER.info("-------------------------------------------------------------");
    LOGGER.info("Got greeting from rabbit... {}", greeting);
    LOGGER.info("-------------------------------------------------------------");

  }

}